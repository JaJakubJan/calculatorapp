import math
from math import floor
from tkinter import StringVar, Tk, Entry, Button

expression = ""


def press(num):
    global expression
    expression = expression_field.get() + str(num)
    expression = expression.replace('NaN', '')
    equation.set(expression)
    expression_field.icursor(len(expression))
    expression_field.focus()


def equal_press(add_fun=None):
    try:
        global expression
        expression = expression_field.get()
        expression = expression.replace('^', '**')
        total = str(eval(expression)) if add_fun is None else str(add_fun(eval(expression)))
        equation.set(total)
        expression = str(total)
        expression_field.icursor(len(expression))
        expression_field.focus()
    except:
        equation.set("NaN")
        expression = ""


def clear():
    global expression
    expression = ""
    equation.set("")


if __name__ == "__main__":
    # Configure window
    GUI_WINDOW = Tk()
    GUI_WINDOW.configure(background="black")
    GUI_WINDOW.title("Calculator")
    GUI_WINDOW.geometry("320x270")
    equation = StringVar()
    expression_field = Entry(GUI_WINDOW, textvariable=equation)
    expression_field.grid(row=1, columnspan=4, ipadx=100)
    equation.set('')
    expression_field.focus()

    # Button settings
    create_button = lambda text, command: Button(GUI_WINDOW,
                                                 text=text,
                                                 fg='black',
                                                 bg='grey',
                                                 command=command,
                                                 height=2,
                                                 width=10)
    # Create buttons
    num_button_lst = []
    command_lst = {0: lambda: press(0),
                   1: lambda: press(1),
                   2: lambda: press(2),
                   3: lambda: press(3),
                   4: lambda: press(4),
                   5: lambda: press(5),
                   6: lambda: press(6),
                   7: lambda: press(7),
                   8: lambda: press(8),
                   9: lambda: press(9), }
    for i in range(0, 10):
        num_button_lst.append(create_button(str(i), command_lst.get(i)))

    execute = Button(GUI_WINDOW, text='exe', fg='black', bg='yellow', command=equal_press, height=1, width=10)
    equal = Button(GUI_WINDOW, text=' = ', fg='black', bg='green', command=equal_press, height=2, width=10)
    clear = Button(GUI_WINDOW, text='Clear', fg='white', bg='red', command=clear, height=2, width=10)
    plus = create_button(' + ', lambda: press("+"))
    minus = create_button(' - ', lambda: press("-"))
    multiply = create_button(' * ', lambda: press("*"))
    divide = create_button(' / ', lambda: press("/"))
    decimal = create_button('.', lambda: press('.'))
    pow = create_button(' ^ ', lambda: press('^'))
    open_br = create_button(' ( ', lambda: press('('))
    close_br = create_button(' ) ', lambda: press(')'))
    sqrt = create_button('sqrt', lambda: equal_press(math.sqrt))
    sin = create_button('sin', lambda: equal_press(math.sin))
    cos = create_button('cos', lambda: equal_press(math.cos))
    ln = create_button('ln', lambda: equal_press(math.log))

    # Grid buttons
    for i in range(0, 10):
        if i == 0:
            num_button_lst[i].grid(row=5, column=0)
        else:
            num_button_lst[i].grid(row=floor((i - 1) / 3 + 2), column=(i - 1) % 3)

    execute.grid(row=1, column=3)
    plus.grid(row=2, column=3)
    minus.grid(row=3, column=3)
    multiply.grid(row=4, column=3)
    pow.grid(row=5, column=1)
    sqrt.grid(row=5, column=2)
    divide.grid(row=5, column=3)
    decimal.grid(row=6, column=0)
    sin.grid(row=6, column=1)
    cos.grid(row=6, column=2)
    ln.grid(row=6, column=3)
    open_br.grid(row=7, column=0)
    close_br.grid(row=7, column=1)
    clear.grid(row=7, column=2)
    equal.grid(row=7, column=3)

    # Run window
    GUI_WINDOW.mainloop()
